-- SUMMARY --

This application has an administrative and front-end section.

Administrative: Manage all data.

Front-End: Read only lists, visible for everyone who visits the website.

-- REQUIREMENTS --

None.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.